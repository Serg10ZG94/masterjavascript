
'use strict'

/*
Programa que pida dos numeros y nos diga cual es el mayor , el menor y si son iguales.
PLUS: Si los numeros no son un número o son menores o iguales a cero , nos los vuelva a pedir.
*/

var num1 = parseInt(prompt("Ingrese su primero número",0));
var num2 = parseInt(prompt("Ingrese el segundo número",0));

/* console.log(typeof num1, typeof num2)
console.log(num1, num2) */

while(isNaN(num1) || isNaN(num2) || (num1 <= 0) || (num2 <=0)){
    num1 = parseInt(prompt("Ingrese su primero número",0));
    num2 = parseInt(prompt("Ingrese el segundo número",0));
}

if(num1==num2){
    alert("LOS NÚMEROS SON IGUALES");
}
else if(num1>num2){
    alert("EL NÚMERO MAYOR ES: "+ num1);
    alert("EL NÚMERO MENOR ES: "+ num2);
}
else if(num2>num1){
    alert("EL NÚMERO MAYOR ES: "+ num2);
    alert("EL NÚMERO MENOR ES: "+ num1);
}
else{
    alert("INTRODUCE NUMEROS CORRECTOS");
}