'use strict'
//VARIABLES 
//Una Variable puede contener información
//Let permite definir variables al ambito donde se usa.
//Var es más global

//Pruebas con Var
var numero=40;
console.log(numero);//valor 40

if(true){
    var numero=50;
    console.log(numero); //valor 50
}

console.log(numero);//valor 50

//Pruebas con Let
let texto="curso js victor robles web";
console.log(texto);//valor curso js victor robles web

if(true){
    let texto="curso js sazg";
    console.log(texto); //valor curso js sazg
}

console.log(texto);//valor curso js sazg