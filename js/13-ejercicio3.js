'use strict'

/*
    Ejercicio 3
    Hacer un programa que muestre todos los números entre 2 números introducidos.
*/

var n1 = Number(prompt("Introduce el primero número"));
var n2 = Number(prompt("Introduce el segundo número"));

do{
    if(n1==n2){
        alert("LOS NÚMEROS SON IGUALES");
    }
    else if(n1>n2){
        alert("LOS NÚMEROS SON: ",n1,n2)
    }
}
while((!Number(n1)&&(!Number(n2))))

//SOLUCIÓN SIN VALIDACIONES

var numero1 = parserInt(prompt("Introduce el primer numero",0));
var numero2 = parserInt(prompt("Introduce el segundo numero",0));

document.write("h1>De "+numero1+" a "+numero2+" están estos números: </h1>");
for(var i=numero1; i<=numero2;i++){
    document.write(i+"<br/>");
}
