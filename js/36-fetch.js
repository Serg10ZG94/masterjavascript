'use strict'

//Fetch (ajax) y peticiones a servicios /apis rest

var div_usuarios = document.querySelector("#usuarios");
var div_janeth = document.querySelector("#janeth");

getUsuarios()
    .then(data => data.json())
    .then(users => {
        listadosUsuarios(users.data);

        return getJaneth();
    })
    .then(data => data.json())
    .then(janeth => {
        mostrarJaneth(janeth.data);
    });



function getUsuarios() {
    return fetch("https://reqres.in/api/users");
}

function getJaneth() {
    return fetch("https://reqres.in/api/users/2");
}

function listadosUsuarios(usuarios) {
    usuarios.map((user, i) => {
        let nombre = document.createElement('h3');
        nombre.innerHTML = i + ". " + user.first_name + " " + user.last_name;
        div_usuarios.appendChild(nombre);
        document.querySelector(".loading").style.display = "none";
    });
}

function mostrarJaneth(usuario) {
    let nombreJaneth = document.createElement('h4');
    nombreJaneth.innerHTML = + user.first_name + " " + user.last_name;
    div_janeth.appendChild(nombreJaneth);
    document.querySelector("#janeth .loading").style.display = "none";
}
