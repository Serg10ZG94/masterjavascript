$(document).ready(function () {
  setInterval(function () {
    location.reload();
  }, 30000);

  //Slider

  $(".bxslider").bxSlider({
    mode: "fade",
    captions: true,
    slideWidth: 1200,
    responsive: true,
  });

  //Posts
  var posts = [
    {
      title: "Prueba de titulo 1",
      date:
        "La Fecha de Publicación fue el día " +
        moment().format("DD") +
        " de " +
        moment().format("MMMM") +
        " del " +
        moment().format("YYYY"),
      content:
        " Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis" +
        "eligendi modi iusto quo perferendis explicabo voluptatibus, atque" +
        "deleniti dignissimos. Eaque ad deleniti necessitatibus. Nulla" +
        "eligendi, quis et laborum dignissimos iste?",
    },
    {
      title: "Prueba de titulo 2",
      date:
        "La Fecha de Publicación fue el día " +
        moment().format("DD") +
        " de " +
        moment().format("MMMM") +
        " del " +
        moment().format("YYYY"),
      content:
        " Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis" +
        "eligendi modi iusto quo perferendis explicabo voluptatibus, atque" +
        "deleniti dignissimos. Eaque ad deleniti necessitatibus. Nulla" +
        "eligendi, quis et laborum dignissimos iste?",
    },
    {
      title: "Prueba de titulo 3",
      date:
        "La Fecha de Publicación fue el día " +
        moment().format("DD") +
        " de " +
        moment().format("MMMM") +
        " del " +
        moment().format("YYYY"),
      content:
        " Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis" +
        "eligendi modi iusto quo perferendis explicabo voluptatibus, atque" +
        "deleniti dignissimos. Eaque ad deleniti necessitatibus. Nulla" +
        "eligendi, quis et laborum dignissimos iste?",
    },
    {
      title: "Prueba de titulo 4",
      date:
        "La Fecha de Publicación fue el día " +
        moment().format("DD") +
        " de " +
        moment().format("MMMM") +
        " del " +
        moment().format("YYYY"),
      content:
        " Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis" +
        "eligendi modi iusto quo perferendis explicabo voluptatibus, atque" +
        "deleniti dignissimos. Eaque ad deleniti necessitatibus. Nulla" +
        "eligendi, quis et laborum dignissimos iste?",
    },
    {
      title: "Prueba de titulo 5",
      date:
        "La Fecha de Publicación fue el día " +
        moment().format("DD") +
        " de " +
        moment().format("MMMM") +
        " del " +
        moment().format("YYYY"),
      content:
        " Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis" +
        "eligendi modi iusto quo perferendis explicabo voluptatibus, atque" +
        "deleniti dignissimos. Eaque ad deleniti necessitatibus. Nulla" +
        "eligendi, quis et laborum dignissimos iste?",
    },
    {
      title: "Prueba de titulo 6",
      date:
        "La Fecha de Publicación fue el día " +
        moment().format("DD") +
        " de " +
        moment().format("MMMM") +
        " del " +
        moment().format("YYYY"),
      content:
        " Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis" +
        "eligendi modi iusto quo perferendis explicabo voluptatibus, atque" +
        "deleniti dignissimos. Eaque ad deleniti necessitatibus. Nulla" +
        "eligendi, quis et laborum dignissimos iste?",
    },
  ];

  posts.forEach((item) => {
    var post = `
    <article class="post">
    <h2>${item.title}</h2>
    <span class="date"> ${item.date}</span>
    <p>
      ${item.content}
    </p>
    <a href="#" class="button-more">Leer Más</a>
  </article>
    `;

    $("#posts").append(post);
  });

  //Selector de Tema
  var theme = $("#theme");

  $("#to-green").click(function () {
    theme.attr("href", "css/green.css");
  });

  $("#to-red").click(function () {
    theme.attr("href", "css/red.css");
  });

  $("#to-blue").click(function () {
    theme.attr("href", "css/blue.css");
  });

  //Scroll arriba de la web
  $(".subir").click(function(e){
    e.preventDefault();

    $("html, body").animate({
      scrollTop:0
    },500);

    return false;
  })

  //Login Falso

  $("#login form").submit(function(){
    var form_name=$("#form_name").val();

    localStorage.setItem("form_name",form_name);
  });

  var form_name=localStorage.getItem("form_name");

  if(form_name!=null && form_name != undefined){
    var about_parrafo= $("#about p");
    about_parrafo.html("<br><strong>Bienvenido," +form_name+"</strong>");
    about_parrafo.append("<a href='#' id='logout'>Cerrar Sesión</a>");
    $("#login").hide();

    $("#logout").click(function(){
      localStorage.clear();
      location.reload();
    })
  }

 
});
